package be.thomasmore.java;

import be.thomasmore.www.entity.ExcelReaderImplementation;
import be.thomasmore.www.entity.ExcelReader;
import be.thomasmore.www.entity.Student;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import org.junit.Assert;
import org.junit.Test;
/**
 * Unit test for simple App.
 * @author deWitPeterPaul
 */
public class AppTest{
    
    private ExcelReader excelReader;
    private File testFile;

    @org.junit.Before
    public void setUp() throws Exception {
        //excelReader = new ExcelReaderDummyImplementation();
        excelReader = new ExcelReaderImplementation();

        ClassLoader classLoader = getClass().getClassLoader();
        testFile = new File(classLoader.getResource("test.xlsx").getFile());
    }

    @Test
    public void testReadStudents() throws Exception {
        Collection<Student> readList = excelReader.readStudents(testFile);
        Collection<Student> hardCodedList = new ArrayList<Student>(){{
            add(new Student("Soete", "Stijn", "stijnsoete@gmail.ru"));
            add(new Student("de Wit", "Peter-Paul", "peterpauldewit@gmail.nk"));
            add(new Student("Plu", "Stef", "StefPlu@hotmail.be"));
        }};

        Assert.assertEquals(readList, hardCodedList);
    }

    @Test
    public void testReadStudentsFail() throws Exception {
        Collection<Student> readList = excelReader.readStudents(testFile);
        Collection<Student> hardCodedList = new ArrayList<Student>(){{
            add(new Student("de Wit", "Peter-Paul Fails", "peterpauldewit@gmail.com"));
        }};

        Assert.assertNotEquals(readList, hardCodedList);
    }
}
