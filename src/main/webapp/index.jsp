<%-- 
    Document   : index
    Created on : 14-okt-2016, 13:13:36
    Author     : Stef
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>


<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>File Upload</title>
    </head>
    <body>
        <h1>File Upload Form</h1> 
        <fieldset>
            <legend>Upload File</legend>
            <form action="UploadServlet" method="post" enctype="multipart/form-data">
                <label for="fileName">Select File: </label>
                <input id="fileName" type="file" name="fileName" size="30"/><br/>            
                <input type="submit" value="Upload"/>
            </form>
        </fieldset>
    </body>
</html>