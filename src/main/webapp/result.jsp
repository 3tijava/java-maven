<%-- 
    Document   : result
    Created on : 16-okt-2016, 21:34:04
    Author     : StijnSoete
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Studenten</title>
    </head>
    <body>
        <h1>Ingelezen studenten!</h1>
        <table>
            <tr>
                <td>naam</td>
                <td>voornaam</td>
                <td>email</td>
            </tr>
            <c:forEach var="student" items="${requestScope.studenten}">
                <tr>
                    <td>${student.lastName}</td>
                    <td>${student.firstName}</td>
                    <td>${student.email}</td>
                </tr>
            </c:forEach>
        </table>

    </body>
</html>
