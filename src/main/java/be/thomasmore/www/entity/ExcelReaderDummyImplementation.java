/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.www.entity;

import java.io.File;
import java.util.Collection;
import java.util.Collections;

/**
 *
 * @author deWitPeter-Paul
 */
public class ExcelReaderDummyImplementation implements ExcelReader{
    public Collection<Student> readStudents(File testFile) {
        return Collections.emptyList();
    }
}
