/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.www.entity;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author deWitPeter-Paul
 */
public class ExcelReaderImplementation implements ExcelReader{
    public Collection<Student> readStudents(File testFile) {
        Collection<Student> students = new ArrayList<Student>();

        FileInputStream file;
        try {
            file = new FileInputStream(testFile);

            XSSFWorkbook workbook = new XSSFWorkbook(file);
            XSSFSheet sheet = workbook.getSheetAt(0);

            for (Row row : sheet) {
                if (row.getRowNum() != 0) {

                    Student student = new Student();

                    for (Cell cell : row) {
                        switch (cell.getColumnIndex()) {
                            case 0:
                                student.setLastName(cell.getStringCellValue());
                                break;
                            case 1:
                                student.setFirstName(cell.getStringCellValue());
                                break;
                            case 2:
                                student.setEmail(cell.getStringCellValue());
                                break;
                            default:
                                break;
                        }
                    }

                    students.add(student);
                }
            }
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return students;
    }
}
