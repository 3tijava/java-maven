
package be.thomasmore.www.entity;

import java.io.File;
import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;


/**
 *
 * @author deWitPeter-Paul
 */
public class StudentService {
    public StudentService(){
        
    }
    Collection<Student> studenten;
    public Collection<Student> leesStudenten(File xlsx){
        EntityManagerFactory emf = null;
        try{
            ExcelReaderImplementation reader = new ExcelReaderImplementation();
            studenten = reader.readStudents(xlsx);
            emf = Persistence.createEntityManagerFactory("be.thomasmore.maven_opdracht2_war_1.0PU"); 
            EntityManager em = emf.createEntityManager(); 
            EntityTransaction tx = em.getTransaction();
            tx.begin();
            for(Student student : studenten){
                em.persist(student);
            }
            tx.commit();
            em.close();
            return studenten;
        }catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                emf.close();
            } catch (NullPointerException e) {
                System.out.println("fout bij aanmaken entitymanagerfactory");
            }
        }
        return studenten;
        
    }
}
