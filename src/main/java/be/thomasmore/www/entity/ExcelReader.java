/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package be.thomasmore.www.entity;

import java.io.File;
import java.util.Collection;

/**
 *
 * @author StijnSoete & deWitPeter-Paul
 */
public interface ExcelReader {
    Collection<Student> readStudents(File testFile);
}
